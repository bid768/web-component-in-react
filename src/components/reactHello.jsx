import React, { PureComponent } from "react";
import "./reactHello.css";

class ReactHello extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      msg: "react组件"
    };
  }

  render() {
    return (
      <div className="box">
        <div className="bigCircle">{this.state.msg}</div>
        <ul className="threeCircle">
          <li></li>
          <li></li>
          <li></li>
        </ul>
      </div>
    );
  }
}

export default ReactHello;
