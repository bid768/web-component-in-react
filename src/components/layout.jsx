import React, { PureComponent } from 'react'
import './layoutWeb.js'
import './layout.css'

class MyLayout extends PureComponent {
  render() {
    return (
      <my-layout>
      <div slot="J_head" class="headSlot"><div class="headCls">这是一个布局web组件</div></div>
      <div slot="J_main">main</div>
      <div slot="J_left">left</div>
      <div slot="J_right">right</div>
      <div slot="J_foot">foot</div>
    </my-layout>
    )
  }
}

export default MyLayout